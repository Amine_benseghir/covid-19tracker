package com.amine.covid19tracker.Controller;

import com.amine.covid19tracker.Entity.StatisticsResponse;
import com.amine.covid19tracker.service.CoronaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthController {


    @Autowired
    private CoronaService coronaService;

    @GetMapping("/")
    public StatisticsResponse health(Model model){
        StatisticsResponse response = coronaService.getStatisticsForCountry("Argentina");
        model.addAttribute("countriesStat", response);
        //coronaService.getStatisticsForCountry("Morocco");

        return response;
    }
}
