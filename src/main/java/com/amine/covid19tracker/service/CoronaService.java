package com.amine.covid19tracker.service;

import com.amine.covid19tracker.Entity.CountriesResponse;
import com.amine.covid19tracker.Entity.StatisticsResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

import java.util.Optional;

import static org.springframework.http.MediaType.APPLICATION_JSON;

@Service
@PropertySource("classpath:APIKeys.properties")
public class CoronaService {
    @Value("${x-rapidapi-key}")
    private String APIKey;

    @Autowired
    private CountriesResponse countriesResponse;

    @PostConstruct
    public CountriesResponse getAllCountries() {
        if (countriesResponse == null) {
            RestTemplate restTemplate = new RestTemplate();
            String url = "https://covid-193.p.rapidapi.com/countries";
            HttpEntity<String> entity = new HttpEntity<String>("parameter", getHeaders());
            ResponseEntity<CountriesResponse> response = restTemplate.exchange(url, HttpMethod.GET, entity, CountriesResponse.class);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody().toString());

            return response.getBody();
        } else
            return countriesResponse;
    }

    public StatisticsResponse  getStatisticsForCountry(String countryName){

            RestTemplate restTemplate = new RestTemplate();
            String url = "https://covid-193.p.rapidapi.com/statistics?country={country}";
            HttpEntity<String> entity = new HttpEntity<String>("parameter", getHeaders());
            ResponseEntity<StatisticsResponse> response = restTemplate.exchange(url, HttpMethod.GET, entity, StatisticsResponse.class,countryName);
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody().toString());
            return response.getBody();
        }






    private HttpHeaders getHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(APPLICATION_JSON);
        headers.add("x-rapidapi-key", APIKey);
        return headers;

    }
}