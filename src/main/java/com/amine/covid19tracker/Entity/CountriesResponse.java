package com.amine.covid19tracker.Entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
@Component
public class CountriesResponse {
    Integer results;
    List<String>response;

    public CountriesResponse() {
    }

    public CountriesResponse(Integer results, List<String> response) {
        this.results = results;
        this.response = response;
    }

    public Integer getResults() {
        return results;
    }

    public void setResults(Integer results) {
        this.results = results;
    }

    public List<String> getResponse() {
        return response;
    }

    public void setResponse(List<String> response) {
        this.response = response;
    }

    @Override
    public String toString() {
        return "CountriesResponse{" +
                "results=" + results +
                ", response=" + response +
                '}';
    }
}
